#!/bin/bash

conky -c ~/.conky/Manjaro-Conky-Files/cpu_ram.conf &
conky -c ~/.conky/Manjaro-Conky-Files/clock.conf &
conky -c ~/.conky/Manjaro-Conky-Files/twitch.conf &
conky -c ~/.conky/Manjaro-Conky-Files/stocks.conf
